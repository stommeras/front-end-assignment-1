const btnGetLoan = document.getElementById("btn-get-a-loan");
const btnWork = document.getElementById("btn-work");
const btnBank = document.getElementById("btn-bank");
const btnRepayLoan = document.getElementById("btn-repay-loan");
const btnBuyLaptop = document.getElementById("btn-buy-laptop");

const spanBankBalance = document.getElementById("span-bank-balance");
const spanOutstandingLoanText = document.getElementById("span-outstanding-loan-text");
const spanOutstandingLoan = document.getElementById("span-outstanding-loan");
const spanWorkBalance = document.getElementById("span-work-balance");

const pLaptopFeatures = document.getElementById("p-laptop-features");
const pLaptopDescription = document.getElementById("p-laptop-description");

const h1LaptopTitle = document.getElementById("h1-laptop-title");
const h2LaptopPrice = document.getElementById("h2-laptop-price");

const selLaptops = document.getElementById("sel-laptops");

const imgLaptop = document.getElementById("img-laptop");
imgLaptop.alt = "Image of computer";
imgLaptop.onerror = () => {
    imgLaptop.src = "/images/no-image.png";
};

const PAY_RATE = 100;
const FETCH_URL = "https://hickory-quilled-actress.glitch.me/";

let bankBalance = 400;
let outstandingLoan = 0;
let workBalance = 0;
let laptopList = [];
let selectedLaptop;

const addLaptopOptions = (laptops) => {
    laptops.forEach((laptop) => addLaptopOption(laptop));
};

const addLaptopOption = (laptop) => {
    const laptopOption = document.createElement("option");
    laptopOption.value = laptop.id;
    laptopOption.appendChild(document.createTextNode(laptop.title));
    selLaptops.appendChild(laptopOption);
};

fetch(`${FETCH_URL}computers`)
    .then((response) => response.json())
    .then((data) => {
        laptopList = data;
        addLaptopOptions(laptopList);
        selectedLaptop = laptopList[0];
        updateLaptopInformation();
    });

/**
 * Formats a number as norwegian currency (NOK) and returns it.
 *
 * @param {number} number The number to format
 * @returns {string} The formatted number
 */
const nokFormat = (number) => {
    return new Intl.NumberFormat("no-NO", {
        style: "currency",
        currency: "NOK",
        currencyDisplay: "symbol",
        maximumFractionDigits: 0,
    }).format(number);
};

/**
 * Updates the balances shown with HTML on the website.
 */
const updateBalances = () => {
    spanBankBalance.innerText = nokFormat(bankBalance);
    spanOutstandingLoan.innerText = nokFormat(outstandingLoan);
    spanWorkBalance.innerText = nokFormat(workBalance);

    if (outstandingLoan == 0) {
        spanOutstandingLoanText.hidden = true;
        spanOutstandingLoan.hidden = true;
        btnRepayLoan.hidden = true;
    } else {
        spanOutstandingLoanText.hidden = false;
        spanOutstandingLoan.hidden = false;
        btnRepayLoan.hidden = false;
    }
};

const getLoan = () => {
    if (!outstandingLoan == 0) {
        return alert(`You already have ${nokFormat(outstandingLoan)} outstanding on your last loan!`);
    }
    loanAmount = Number(prompt("How big a loan do you want?"));
    if (loanAmount > 2 * bankBalance) {
        return alert(`The biggest loan you can get at the moment is ${nokFormat(2 * bankBalance)}.`);
    }

    bankBalance += loanAmount;
    outstandingLoan += loanAmount;
    updateBalances();
};

const doWork = () => {
    workBalance += PAY_RATE;
    updateBalances();
};

/**
 * Deposits work balance into the bank, taxing 10% if there is an outstanding loan
 */
const payBank = () => {
    let loanTax = 0.1 * workBalance;
    let depositAmount = 0.9 * workBalance;

    if (loanTax > outstandingLoan) {
        let rest = loanTax - outstandingLoan;
        outstandingLoan = 0;
        depositAmount += rest;
    } else {
        outstandingLoan -= loanTax;
    }

    bankBalance += depositAmount;
    workBalance = 0;
    updateBalances();
};

/**
 * Pays the outstanding loan from work balance, depositing anything leftover in the bank
 */
const repayLoan = () => {
    if (workBalance > outstandingLoan) {
        let rest = workBalance - outstandingLoan;
        outstandingLoan = 0;
        bankBalance += rest;
    } else {
        outstandingLoan -= workBalance;
    }

    workBalance = 0;
    updateBalances();
};

/**
 * Changes the selected laptop when select element changes
 *
 * @param {e} e The current event
 */
const changeLaptopSelection = (e) => {
    selectedLaptop = laptopList[e.target.selectedIndex]
    updateLaptopInformation();
};

/**
 * Updates the laptop information on the page based on selected laptop
 *
 */
const updateLaptopInformation = () => {
    imgLaptop.src = FETCH_URL + selectedLaptop.image;

    pLaptopFeatures.innerText = selectedLaptop.specs.join("\n");
    h1LaptopTitle.innerText = selectedLaptop.title;
    pLaptopDescription.innerText = selectedLaptop.description;
    h2LaptopPrice.innerText = nokFormat(selectedLaptop.price);
};

const buyLaptop = () => {
    if (selectedLaptop.price > bankBalance) {
        return alert(`You are missing ${nokFormat(selectedLaptop.price - bankBalance)} to be able to afford this laptop.`);
    }
    bankBalance -= selectedLaptop.price;
    updateBalances();
    alert(`You are now the owner of a brand new ${selectedLaptop.title}!`);
};

btnGetLoan.addEventListener("click", getLoan);
btnWork.addEventListener("click", doWork);
btnBank.addEventListener("click", payBank);
btnRepayLoan.addEventListener("click", repayLoan);
btnBuyLaptop.addEventListener("click", buyLaptop);
selLaptops.addEventListener("change", changeLaptopSelection);

window.onload = () => updateBalances();
